package javaTemplate.steps;

import cucumber.api.Scenario;
import javaTemplate.cucumber.BaseStep;
import javaTemplate.cucumber.ScenarioContext;
import javaTemplate.pages.Expedia;
import javaTemplate.pages.ExpediaResults;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import static junit.framework.TestCase.assertTrue;

public class ExpediaTest extends BaseStep {

    private Expedia expedia;
    private ExpediaResults expediaResults;

    public ExpediaTest(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;

        After(new String[] {"@Example1"}, (Scenario scenario) -> super.teardown(scenario));

        When("^User clicks \"([^\"]*)\" button in Expedia Page$", (String button) -> {
            expedia = new Expedia(scenarioContext.getDriver());
            expedia.getButtonByName(button).click();
        });

        Then("^Enter \"([^\"]*)\" in Origin text field$", (String keyword) -> {
            expedia.getOriginField().clear();
            expedia.getOriginField().sendKeys(keyword);
        });

        And("^Enter \"([^\"]*)\" in Destination text field$", (String keyword) -> {
            expedia.getDestiantionField().clear();
            expedia.getDestiantionField().sendKeys(keyword);
        });

        Then("^Enter \"([^\"]*)\" in departing field$", (String departing) -> {
            expedia.getDepartingField().sendKeys(departing);
            expedia.waitDatePickerCal();
            expedia.getCloseDatePicker().click();
        });

        And("^Enter \"([^\"]*)\" in returning field$", (String returning) -> {
            expedia.getReturningField().sendKeys(returning);
            expedia.getCloseDatePicker().click();
        });

        When("^User clicks on Search button$", () -> {
            String winHandle="";
            expedia.getSearchButton().click();
            expediaResults = new ExpediaResults(scenarioContext.getDriver());
            for(String window: scenarioContext.getDriver().getWindowHandles()) {
                scenarioContext.getDriver().switchTo().window(window);
                if (scenarioContext.getDriver().getTitle().contains("Expedia"))
                    winHandle=window;
            }
            scenarioContext.getDriver().switchTo().window(winHandle);
        });

        Then("^Select \"([^\"]*)\" in Sort Dropdown$", (String value) -> {
            Select select = new Select(expediaResults.getSortDropdown());
            expediaResults.getSortDropdown().click();
            for(WebElement option: select.getOptions()){
               if(option.getText().equals(value))
                   option.click();
            }
            expediaResults.waitForResults();
        });

        And("^Validate prices are sorted from highest to lowest$", () -> {
            int currentItem;
            int nextItem;
            for(int i=0; i<expediaResults.getPricesList().size()-1; i++){
                currentItem = Integer.parseInt(expediaResults.getPricesList().get(i).getText().replace("$","").replace(",",""));
                nextItem = Integer.parseInt(expediaResults.getPricesList().get(i+1).getText().replace("$","").replace(",",""));
                assertTrue(currentItem>=nextItem);
            }
        });
    }
}