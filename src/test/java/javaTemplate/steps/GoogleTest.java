package javaTemplate.steps;

import javaTemplate.cucumber.BaseStep;
import javaTemplate.cucumber.ScenarioContext;
import javaTemplate.pages.Google;
import cucumber.api.Scenario;

public class GoogleTest extends BaseStep {

    private Google google;

    public GoogleTest(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;

        After(new String[] {"@GoogleTest"}, (Scenario scenario) -> super.teardown(scenario));

        Given("User navigates to \"([^\"]*)\"$", (String url) -> {
            super.setup(url);
            google = new Google(scenarioContext.getDriver());
        });

        When("^User searches for \"([^\"]*)\"$", (String keyword) -> {
            if(!google.getEnglishLink().isEmpty()) {
                google.getEnglishLink().get(0).click();
            }
            google.getSearchField().sendKeys(keyword);
            google.getGoogleSearchButton().click();
        });

        Then("^Click on \"([^\"]*)\" tab$", (String tab) -> {
            google.getTabByName(tab).click();
        });
    }
}