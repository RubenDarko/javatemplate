@Automation
Feature: Google Test

  @GoogleTest
  Scenario: Google Test
    Given User navigates to "https://www.google.com"
    When User searches for "Pink Floyd"
    Then Click on "Images" tab
    And Click on "Videos" tab