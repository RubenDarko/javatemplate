@Expedia
Feature: Expedia

  @Example1
  Scenario: Example1
    Given User navigates to "https://www.expedia.com/"
    When User clicks "Flights" button in Expedia Page
    Then Enter "Aguascalientes, Aguascalientes, Mexico (AGU-Licenciado Jesus Teran Peredo Intl.)" in Origin text field
    And Enter "Cancun, Quintana Roo, Mexico (CUN-Cancun Intl.)" in Destination text field
    Then Enter "02/19/2020" in returning field
    And Enter "02/16/2020" in departing field
    When User clicks on Search button
    Then Select "Price (Highest)" in Sort Dropdown
    And Validate prices are sorted from highest to lowest