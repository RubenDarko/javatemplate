package javaTemplate.cucumber;

import cucumber.api.Scenario;
import cucumber.api.java8.En;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class BaseStep implements En {

    public Path currentRelativePath = Paths.get("");
    public String currentWorkingDirectory = currentRelativePath.toAbsolutePath().toString();
    public String downloadFilePath = currentWorkingDirectory + "/downloads/";
    protected ScenarioContext scenarioContext;

    public void setup(String url) {
        ChromeOptions options = new ChromeOptions();
        HashMap<String, Object> chromePrefs = new HashMap<>();

        chromePrefs.put("download.default_directory", downloadFilePath);
        options.addArguments("--incognito");
        options.setExperimentalOption("prefs", chromePrefs);
        scenarioContext.setDriver(new ChromeDriver(options));
        scenarioContext.getDriver().get(url);
        scenarioContext.getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        scenarioContext.getDriver().manage().window().maximize();
    }

    public void teardown(Scenario scenario) {
        final byte[] screenshot = ((TakesScreenshot) scenarioContext.getDriver()).getScreenshotAs(OutputType.BYTES);
        if (scenario.isFailed())
            scenario.embed(screenshot, "image/png");
        scenarioContext.getDriver().quit();
    }
}