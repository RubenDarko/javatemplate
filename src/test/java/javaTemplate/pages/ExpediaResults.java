package javaTemplate.pages;

import javaTemplate.cucumber.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

public class ExpediaResults extends BasePage {

    public ExpediaResults(WebDriver driver) {
        super(driver);
    }

    @FindAll(@FindBy(how = How.XPATH, using="//ul[@class='segmented-list results-list price-sort']"))
    private List<WebElement> flightsListSort;

    @FindAll(@FindBy(how = How.XPATH, using="//ul[@class='segmented-list results-list']"))
    private List<WebElement> flightsList;

    @FindBy(how = How.XPATH, using="//select[@id='sortDropdown']")
    private WebElement sortDropdown;

    @FindAll(@FindBy(how = How.XPATH, using="//span[@data-test-id='listing-price-dollars']"))
    private List<WebElement> pricesList;

    public List<WebElement> getFlightsListSort() {
        return flightsListSort;
    }

    public List<WebElement> getFlightsList() {
        return flightsList;
    }

    public WebElement getSortDropdown() {
        return sortDropdown;
    }

    public List<WebElement> getPricesList() {
        return pricesList;
    }

    public void waitForResults() throws Throwable {
        while(getFlightsList().isEmpty())
            driver.wait(100);
        while(getFlightsListSort().isEmpty())
            driver.wait(100);
    }
}