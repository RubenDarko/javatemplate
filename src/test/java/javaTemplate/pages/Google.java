package javaTemplate.pages;

import javaTemplate.cucumber.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class Google extends BasePage {

    @FindBy(how = How.XPATH, using = "//input[@title='Search']")
    private WebElement searchTextField;

    @FindBy(how = How.XPATH, using = "//input[@name='btnK']")
    private WebElement googleSearchButton;

    @FindAll(@FindBy(how = How.XPATH, using = "//a[text()='English']"))
    private List<WebElement> englishLink;

    public Google(WebDriver driver) {
        super(driver);
    }

    public WebElement getSearchField() {
        return searchTextField;
    }

    public WebElement getGoogleSearchButton() {
        return googleSearchButton;
    }

    public List<WebElement> getEnglishLink() {
        return englishLink;
    }

    public WebElement getTabByName(String name) {
        By googleTab = By.xpath("//a[contains(text(),'" + name + "')]");
        return driver.findElement(googleTab);
    }
}