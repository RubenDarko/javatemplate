package javaTemplate.pages;

import javaTemplate.cucumber.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import java.util.List;

public class Expedia extends BasePage {

    public Expedia(WebDriver driver) {
        super(driver);
    }

    @FindBy(how = How.ID, using="flight-origin-hp-flight")
    private WebElement originField;

    @FindBy(how = How.ID, using="flight-destination-hp-flight")
    private WebElement destiantionField;

    @FindAll(@FindBy(how = How.XPATH, using="//span[text()='Prices are round trip per person in USD.']"))
    private List<WebElement> datePickerCal;

    @FindAll(@FindBy(how = How.XPATH, using= "//button[@class='datepicker-close-btn close btn-text']"))
    private List<WebElement> closeDatePickers;

    @FindBy(how = How.XPATH, using= "//button[@class='datepicker-close-btn close btn-text']")
    private WebElement closeDatePicker;

    @FindBy(how = How.ID, using="flight-departing-hp-flight")
    private WebElement departingField;

    @FindBy(how = How.ID, using="flight-returning-hp-flight")
    private WebElement returningField;

    @FindBy(how = How.XPATH, using="//button[@class='btn-primary btn-action gcw-submit']")
    private WebElement searchButton;

    public WebElement getButtonByName(String button) {
        By element = By.xpath("//span[@class='tab-label'][contains(text(),'" + button + "')]");
        return driver.findElement(element);
    }

    public void waitDatePickerCal() throws Throwable {
        while(datePickerCal.isEmpty()) {
            System.out.println(datePickerCal.size());
            driver.wait(200);
        }
        while(closeDatePickers.isEmpty()) {
            System.out.println(closeDatePickers.size());
            driver.wait(200);
        }
    }

    public WebElement getCloseDatePicker() {
        return closeDatePicker;
    }

    public WebElement getDestiantionField() {
        return destiantionField;
    }

    public WebElement getOriginField() {
        return originField;
    }

    public WebElement getReturningField() {
        return returningField;
    }

    public WebElement getDepartingField() {
        return departingField;
    }

    public WebElement getSearchButton() {
        return searchButton;
    }
}