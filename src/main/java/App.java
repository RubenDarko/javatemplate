import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

public class App {
    public static void main(String[] args) {

        String mpCryptoPassword = "remoteDriver";
        String value = "mypwd";
        System.out.println("Original Value : " + value);
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(mpCryptoPassword);
        String encryptedPassword = encryptor.encrypt(value);
        System.out.println(encryptedPassword);

    }
}